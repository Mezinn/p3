const Timer = require('./Timer');
const Window = require('./Window');
const SnakeFactory = require('./SnakeFactory');
const SnakeMoveHandler = require('./SnakeMoveHandler');

class Game {
    constructor(config) {
        this.timer = new Timer(1000 / config.speed);
        this.window = new Window(config.field.size);
        this.snakeFactory = new SnakeFactory(this.window);
        this.snakeMoveHandler = new SnakeMoveHandler(this.window);

        this.snakes = new Map();

        this.timer.addListener(function () {

            for (let snake of this.snakes.values()) {
                this.snakeMoveHandler.handle(snake);
            }

        }.bind(this));
    }

    run() {
        this.timer.start();
    }

    launch(uid, handler) {

        let snake = this.snakeFactory.create();

        this.snakes.set(uid, snake);

        this.timer.addListener(handler.bind(null, snake));
    }
}

module.exports = Game;
