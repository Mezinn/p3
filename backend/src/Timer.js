class Timer {

    constructor(ms) {
        this.ms = ms;
        this.listeners = [];
    }

    addListener(handler) {
        this.listeners.push(handler);
    }

    start() {
        this.timer = setInterval(function () {
            this.listeners.forEach(function (listener) {
                listener();
            });
        }.bind(this), this.ms);
    }

    end() {
        clearInterval(this.timer);
    }
}

module.exports = Timer;
