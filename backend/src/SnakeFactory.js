const PalleteFactory = require('./PalleteFactory');
const Snake = require('./Snake');
const Directions = require('./Directions');
const faker = require('faker');

const palleteFactory = new PalleteFactory();

function randDirection() {
    return Object.values(Directions)[Math.ceil(Math.random() * 4)];
}

class SnakeFactory {

    constructor(window) {
        this.window = window;
        this.used = new Set();
    }

    create() {
        let name = '';
        do {
            name = faker.name.firstName();
        } while (this.used.has(name) !== false);

        this.used.add(name);

        return new Snake(
            name,
            palleteFactory.rand(),
            [
                this.window.rand(),
            ],
            randDirection(),
        );
    }
}

module.exports = SnakeFactory;
