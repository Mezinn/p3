const Point = require('./Point');

class SnakeMoveHandler {

    constructor(window) {
        this.window = window;
    }

    handle(snake) {

        const body = snake.getBody();

        const dir = snake.getDir();

        let head = body[0];

        body.unshift(head.add(dir));

        body.pop();
    }
}
module.exports = SnakeMoveHandler;
