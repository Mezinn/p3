const Point = require('./Point');

class Window {

    constructor(size) {
        this.size = size;
    }

    getSize() {
        return this.size;
    }

    rand() {
        return new Point(
            Math.ceil(Math.random() * this.size),
            Math.ceil(Math.random() * this.size)
        );
    }
}

module.exports = Window;
