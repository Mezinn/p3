const faker = require('faker');
const Pallete = require('./Pallete');

class PalleteFactory {

    constructor() {
        this.used = new Set(['white']);
    }

    rand() {

        let head = '';
        let body = '';

        do {
            head = faker.commerce.color();
        } while (this.used.has(head) !== false);

        this.used.add(head);

        do {
            body = faker.commerce.color();
        } while (this.used.has(body) !== false);

        this.used.add(body);

        return new Pallete(head, body);
    }
}

module.exports = PalleteFactory;
