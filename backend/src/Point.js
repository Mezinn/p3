class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    add(another) {
        return new Point(
            this.x + another.x,
            this.y + another.y
        );
    }
}

module.exports = Point;
