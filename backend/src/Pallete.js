class Pallete {
    constructor(head, body) {
        this.head = head;
        this.body = body;
    }

    getBody() {
        return this.body;
    }

    getHead() {
        return this.head;
    }
}

module.exports = Pallete;
