class Snake {
    constructor(
        name,
        pallete,
        body,
        dir
    ) {
        this.name = name;
        this.pallete = pallete;
        this.body = body;
        this.dir = dir;
    }

    getName() {
        return this.name;
    }

    getPallete() {
        return this.pallete;
    }

    getBody() {
        return this.body;
    }

    getDir() {
        return this.dir;
    }
}

module.exports = Snake;
