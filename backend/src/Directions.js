const Point = require('./Point');

module.exports = {
    'ArrowLeft': new Point(-1, 0),
    'ArrowRight': new Point(1, 0),
    'ArrowDown': new Point(0, 1),
    'ArrowUp': new Point(0, -1),
}


