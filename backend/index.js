const Game = require('./src/Game');

const server = require('http').createServer();
const io = require('socket.io')(server);

console.log('Server running on 3000');


server.listen(3000);

const config = {
    speed: 1,
    field: {
        size: 20
    }
};

const game = new Game(config);

game.run();

io.on('connection', function (socket) {
    game.launch(socket.id, function (snake) {
        socket.emit('sync', JSON.stringify(snake));
    })
});
